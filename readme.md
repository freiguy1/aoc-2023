A repo for [Advent of Code](https://adventofcode.com) 2023 work. I'll be doing the first handful of puzzles using [zig](https://ziglang.org)! The only requirement is zig 0.11.

`zig build run -- [day] [puzzle]`

For example: `zig build run -- 12 2`

Both params are optional. If you supply neither, all puzzles will get ran. If you only supply a day, both puzzles for that day will be ran.

#### Helpful links
- [Zig 0.11 Language Reference](https://ziglang.org/documentation/0.11.0/)
- [Zig Learn](https://ziglearn.org/)
- [Zig 0.11 Release Notes](https://ziglang.org/download/0.11.0/release-notes.html)
- [Zig std lib docs](https://ziglang.org/documentation/master/std/)
- [My repo for AoC 2021](https://gitlab.com/freiguy1/aoc-2021)
- [Learning Zig Book](https://www.openmymind.net/learning_zig/)
