const std = @import("std");
const debug = std.debug;

pub fn puzzle1() !void {
    debug.print("Day ? puzzle 1 unimplemented!\n", .{});
}

pub fn puzzle2() !void {
    debug.print("Day ? puzzle 2 unimplemented!\n", .{});
}
