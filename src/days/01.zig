const std = @import("std");
const util = @import("../util.zig");

const input = @embedFile("../data/01.txt");

pub fn puzzle1() !void {
    var total: usize = 0;
    var iter = std.mem.split(u8, input, "\n");
    while (iter.next()) |line| {
        var digits = [_]?u8{ null, null };
        for (line) |char| {
            if (char - 48 < 10) {
                digits[1] = char - 48;
                if (digits[0] == null) digits[0] = char - 48;
            }
        }
        if (digits[0]) |d| total += d * 10;
        if (digits[1]) |d| total += d;
    }
    std.log.info("Day 1 puzzle 1: {d}", .{total});
}

pub fn puzzle2() !void {
    var total: usize = 0;
    var iter = std.mem.split(u8, input, "\n");
    while (iter.next()) |line| {
        var digits = [_]?u8{ null, null };
        for (line, 0..) |_, index| {
            const maybe_digit = isDigit(line[index..]);
            if (maybe_digit) |digit| {
                digits[1] = digit;
                if (digits[0] == null) digits[0] = digit;
            }
        }
        if (digits[0]) |d| total += d * 10;
        if (digits[1]) |d| total += d;
    }
    std.log.info("Day 1 puzzle 2: {d}", .{total});
}

const spelled_digits = [_][]const u8{
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
};

fn isDigit(str: []const u8) ?u8 {
    // The minus 48 here just deals with ascii. 0 in ascii is 48th byte. Simple conversion.
    if (str.len == 0) return null else if (str[0] - 48 < 10) return str[0] - 48;

    for (spelled_digits, 1..) |spelled_digit, value| {
        if (std.mem.startsWith(u8, str, spelled_digit)) {
            return @intCast(value);
        }
    }
    return null;
}
