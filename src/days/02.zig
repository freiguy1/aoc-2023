const std = @import("std");
const util = @import("../util.zig");

const input = @embedFile("../data/02.txt");

const max = .{ .red = 12, .green = 13, .blue = 14 };

const Result = struct {
    red: usize = 0,
    green: usize = 0,
    blue: usize = 0,

    fn parse(str: []const u8) Result {
        var result = Result{};
        var comma_iter = std.mem.split(u8, str, ", ");
        while (comma_iter.next()) |num_and_color| {
            var num_color_iter = std.mem.split(u8, num_and_color, " ");
            const num = std.fmt.parseUnsigned(usize, num_color_iter.next().?, 10) catch unreachable;
            const color = num_color_iter.next().?;
            if (std.mem.eql(u8, color, "red")) {
                result.red = num;
            } else if (std.mem.eql(u8, color, "green")) {
                result.green = num;
            } else if (std.mem.eql(u8, color, "blue")) {
                result.blue = num;
            }
        }
        return result;
    }

    fn isValid(self: Result) bool {
        return self.red <= max.red and self.green <= max.green and self.blue <= max.blue;
    }
};

pub fn puzzle1() !void {
    var total: usize = 0;
    var line_iter = std.mem.split(u8, input, "\n");
    var line_index: usize = 1;
    outer: while (line_iter.next()) |line| : (line_index += 1) {
        if (line.len == 0) continue;
        const results = line[std.mem.indexOfScalar(u8, line, ':').? + 2 ..];
        var results_iter = std.mem.split(u8, results, "; ");
        while (results_iter.next()) |result| {
            const parsed = Result.parse(result);
            if (!parsed.isValid()) continue :outer;
        }
        total += line_index;
    }
    std.log.info("Day 2 puzzle 1: {d}", .{total});
}

pub fn puzzle2() !void {
    var total: usize = 0;
    var line_iter = std.mem.split(u8, input, "\n");
    while (line_iter.next()) |line| {
        if (line.len == 0) continue;
        const results = line[std.mem.indexOfScalar(u8, line, ':').? + 2 ..];
        var results_iter = std.mem.split(u8, results, "; ");
        var highest_red: usize = 0;
        var highest_green: usize = 0;
        var highest_blue: usize = 0;
        while (results_iter.next()) |result| {
            const parsed = Result.parse(result);
            if (parsed.red > highest_red) highest_red = parsed.red;
            if (parsed.green > highest_green) highest_green = parsed.green;
            if (parsed.blue > highest_blue) highest_blue = parsed.blue;
        }
        total += highest_red * highest_green * highest_blue;
    }
    std.log.info("Day 2 puzzle 2: {d}", .{total});
}
