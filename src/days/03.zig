const std = @import("std");
const util = @import("../util.zig");

const input = @embedFile("../data/03.txt");
const width: usize = std.mem.indexOfScalar(u8, input, '\n').?;
const height: usize = input.len / (width + 1);

pub fn puzzle1() !void {
    // std.log.info("width: {d}, height: {d}, isSymbol: {any}", .{ width, height, isSymbol(6, 1) });
    var sum: usize = 0;
    var iter = NumberFinderIter{};
    while (iter.next()) |num| {
        // std.log.info("{any}, {any}", .{ num, num.adjacentToSymbol() });
        if (num.adjacentToSymbol()) sum += num.number;
    }

    std.log.info("Day 3 puzzle 1: {d}", .{sum});
}

fn isSymbol(x: usize, y: usize) bool {
    const index = y * (width + 1) + x;
    if (x > width - 1 or y > height - 1 or index > input.len) return false;
    return input[y * (width + 1) + x] != '.';
}

const FoundNumber = struct {
    x: usize = 0,
    y: usize = 0,
    digits: usize = 0,
    number: usize = 0,

    fn adjacentToSymbol(self: FoundNumber) bool {
        // std.log.info("{d}: left({any}), right({}), top({}), bot({})", .{ self.number, self.checkLeft(), self.checkRight(), self.checkTop(), self.checkBottom() });
        return self.checkLeft() or self.checkRight() or self.checkTop() or self.checkBottom();
    }

    // checks upper left, left, and lower left
    fn checkLeft(self: FoundNumber) bool {
        if (self.x == 0) return false;
        return isSymbol(self.x - 1, self.y -| 1) or isSymbol(self.x - 1, self.y) or isSymbol(self.x - 1, self.y + 1);
    }

    // checks upper right, right, and lower right
    fn checkRight(self: FoundNumber) bool {
        if (self.x == width) return false;
        return isSymbol(self.x + self.digits, self.y -| 1) or isSymbol(self.x + self.digits, self.y) or isSymbol(self.x + self.digits, self.y + 1);
    }

    // checks only the spaces above the digits
    fn checkTop(self: FoundNumber) bool {
        if (self.y == 0) return false;
        for (0..self.digits) |i| {
            if (isSymbol(self.x + i, self.y - 1))
                return true;
        }
        return false;
    }

    // checks only the spaces below the digits
    fn checkBottom(self: FoundNumber) bool {
        if (self.y == height - 1) return false;
        for (0..self.digits) |i| {
            if (isSymbol(self.x + i, self.y + 1))
                return true;
        }
        return false;
    }

    fn isAdjacentTo(self: FoundNumber, p: Point) bool {
        // std.log.info("isAdjacentTo {any}, {any}", .{ self, p });
        //check left
        if (self.x > 0) {
            if (p.x == self.x - 1 and (p.y == self.y -| 1 or p.y == self.y or p.y == self.y + 1)) return true;
        }

        // check right
        if (p.x == self.x + self.digits and (p.y == self.y -| 1 or p.y == self.y or p.y == self.y + 1)) return true;

        // check top
        if (self.y > 0) {
            for (0..self.digits) |i| {
                if (self.x + i == p.x and p.y == self.y - 1) return true;
            }
        }

        // check bot
        for (0..self.digits) |i| {
            if (self.x + i == p.x and p.y == self.y + 1) return true;
        }

        return false;
    }
};

const NumberFinderIter = struct {
    y: usize = 0,
    x: usize = 0,
    max_y: usize = std.math.maxInt(usize),

    fn next(self: *NumberFinderIter) ?FoundNumber {
        var result = FoundNumber{};
        for (input[self.y * (width + 1) + self.x ..]) |char| {
            if (self.y > self.max_y) {
                return null;
            }

            if (util.charToDigit(char)) |digit| {
                if (result.number == 0) {
                    result.x = self.x;
                    result.y = self.y;
                }
                result.number = result.number * 10 + digit;
                result.digits += 1;
                self.x += 1;
            } else {
                if (char == '\n') {
                    self.y += 1;
                    self.x = 0;
                } else {
                    self.x += 1;
                }

                if (result.number != 0) {
                    return result;
                }
            }
        }
        return null;
    }
};

// ------------------------------------------

pub fn puzzle2() !void {
    var iter = StarIter{};
    var sum: usize = 0;
    while (iter.next()) |star| {
        var num_iter = NumberFinderIter{ .y = star.y -| 1, .max_y = star.y + 1 };
        var adj_1: ?FoundNumber = null;
        var adj_2: ?FoundNumber = null;
        while (num_iter.next()) |num| {
            const is_adjacent = num.isAdjacentTo(star);
            if (is_adjacent and adj_1 == null) {
                adj_1 = num;
            } else if (is_adjacent and adj_2 == null) {
                adj_2 = num;
                break;
            }
        }

        if (adj_1 != null and adj_2 != null) {
            sum += (adj_1.?.number * adj_2.?.number);
        }
    }
    std.log.info("Day 3 puzzle 2: {d}", .{sum});
}

const Point = struct { x: usize, y: usize };

const StarIter = struct {
    x: usize = 0,
    y: usize = 0,

    fn next(self: *StarIter) ?Point {
        for (input[self.y * (width + 1) + self.x ..]) |char| {
            if (char == '*') {
                self.x += 1;
                return Point{ .x = self.x - 1, .y = self.y };
            } else if (char == '\n') {
                self.x = 0;
                self.y += 1;
            } else {
                self.x += 1;
            }
        }
        return null;
    }
};
