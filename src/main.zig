const clap = @import("clap");
const std = @import("std");
const day_01 = @import("days/01.zig");
const day_02 = @import("days/02.zig");
const day_03 = @import("days/03.zig");
const day_empty = @import("days/empty.zig");

const debug = std.debug;
const io = std.io;

pub fn main() anyerror!void {
    const params = comptime clap.parseParamsComptime(
        \\-h, --help            Display this help and exit.
        \\<str>...
    );

    var res = try clap.parse(clap.Help, &params, clap.parsers.default, .{});
    defer res.deinit();

    if (res.args.help != 0) {
        debug.print(
            \\Usage: aoc-2023 [day] [puzzle-number]
            \\    day: optional [1-25]
            \\    puzzle-number: optional [1|2]
            \\
        , .{});
        return;
        // return clap.help(io.getStdErr().writer(), clap.Help, &params, .{});
    }

    const dayNum = try parseDayNum(res.positionals);
    const puzzleNum = try parsePuzzleNum(res.positionals);
    if (puzzleNum != null and puzzleNum.? > 2) {
        debug.print("Puzzle number too high\n", .{});
        return error.InvalidPuzzleNum;
    }

    if (dayNum != null and dayNum.? > puzzles.len) {
        debug.print("Day number too high\n", .{});
        return error.InvalidDayNum;
    }

    // Run one or more puzzles using args
    if (dayNum) |dn| {
        if (puzzleNum) |pn| {
            try puzzles[dn - 1][pn - 1]();
        } else {
            try puzzles[dn - 1][0]();
            try puzzles[dn - 1][1]();
        }
    } else {
        for (puzzles) |puzzle| {
            try puzzle[0]();
            try puzzle[1]();
        }
    }
}

fn parseDayNum(positionals: []const []const u8) !?u8 {
    if (positionals.len == 0)
        return null;

    return std.fmt.parseUnsigned(u8, positionals[0], 10) catch return error.InvalidDayNum;
}

fn parsePuzzleNum(positionals: []const []const u8) !?u8 {
    if (positionals.len < 2)
        return null;

    return std.fmt.parseUnsigned(u8, positionals[1], 10) catch return error.InvalidPuzzleNum;
}

const puzzles = [_][2]*const fn () anyerror!void{
    [2]*const fn () anyerror!void{ day_01.puzzle1, day_01.puzzle2 },
    [2]*const fn () anyerror!void{ day_02.puzzle1, day_02.puzzle2 },
    [2]*const fn () anyerror!void{ day_03.puzzle1, day_03.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
    [2]*const fn () anyerror!void{ day_empty.puzzle1, day_empty.puzzle2 },
};
