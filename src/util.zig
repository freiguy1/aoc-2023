const std = @import("std");
const fs = std.fs;
const parseUnsigned = std.fmt.parseUnsigned;

pub const FileLineIterator = struct {
    file: fs.File,
    line_buffer: [8192]u8 = undefined,

    pub fn next(self: *FileLineIterator) ?[]u8 {
        return self.file.reader().readUntilDelimiterOrEof(self.line_buffer[0..], '\n') catch null;
    }
};

pub const FileLineUintIterator = struct {
    iter: FileLineIterator,

    pub fn new(file: fs.File) @This() {
        return FileLineUintIterator{ .iter = FileLineIterator{ .file = file } };
    }

    pub fn next(self: *FileLineUintIterator) ?u32 {
        if (self.iter.next()) |buf| {
            return std.fmt.parseUnsigned(u32, buf, 10) catch null;
        } else return null;
    }
};

pub fn parseDelimitedUnsigned(comptime T: type, text: []const u8, delimiter: []const u8, allocator: *std.mem.Allocator) !std.ArrayList(T) {
    var numbers = std.ArrayList(T).init(allocator);
    var number_iter = std.mem.split(text, delimiter);
    while (number_iter.next()) |val| {
        try numbers.append(try parseUnsigned(T, val, 10));
    }
    return numbers;
}

pub fn readSingleLine(file_name: []const u8) ![]u8 {
    var file = try fs.cwd().openFile(file_name, .{});
    defer file.close();
    var line_buffer: [8192]u8 = undefined;
    return (try file.reader().readUntilDelimiterOrEof(&line_buffer, '\n')).?;
}

pub fn charToDigit(char: u8) ?u8 {
    return if (char > 47 and char < 58) char - 48 else null;
}
